import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

void main() {
  runApp(const FungsiUtama());
}

class FungsiUtama extends StatelessWidget {
  const FungsiUtama({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: Text("App Bar Hello Word!")),
    ));
  }
}
